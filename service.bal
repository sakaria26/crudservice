import ballerina/http;

public type Course record {|
    readonly string code;
    string name;
    int credit;
    string[] topics;
|};

public type ConflictingCourseCode record {|
    *http:Conflict;
    ErrorMessage body;
|};

public type ErrorMessage record {|
    string errorMessage;
|};

//Changing the port number
configurable int port = 9090;

//Defines inheritance
public type CreatedCourse record {|
    *http:Created;
    Course body;
|};

public type CourseNotFound record {|
    *http:NotFound;
    ErrorMessage body;
|};

public type CoursePart record {|
    string name?;
    int credit?;
    string[] topics?;
|};

service /coursesoutline/api/v1 on new http:Listener(port) {
    //initialize function to declare table within the service
    private table<Course> key(code) courses;
    function init() {
        self.courses = table [];
    }

    //to get all the courses with the self prefix
    resource function get all() returns Course[] {

        return self.courses.toArray();
    }

    //using curl to get all the courses
    //curl http://localhost:9090/courses

    //posting a new course
    resource function post courses(@http:Payload Course newCourses) returns CreatedCourse|ConflictingCourseCode {
        string[] existingCourseCodes = from var {code} in self.courses
            where code == newCourses.code
            select code;

        if existingCourseCodes.length() > 0 {
            return <ConflictingCourseCode>{
                body: {
                    errorMessage: string `Error: a course with code ${newCourses.code} already exists`
                }
            };
        } else {
            return <CreatedCourse>{body: newCourses};
        }
    }

    resource function get courses/[string code]() returns Course|CourseNotFound {
        Course? theCourse = self.courses[code];
        if theCourse == () {
            return <CourseNotFound>{
                body: {errorMessage: string `Error no course with code ${code}`}
            };
        } else {
            return theCourse;
        }
    }

    resource function put course/[string code](@http:Payload CoursePart coursePart) returns CourseNotFound|Course {
        Course? theCourse = self.courses[code];
        
        if theCourse == (){
            return <CourseNotFound>{
                body: {errorMessage: string `Error no course with code ${code}`}
            };
        }else{
            string? name = coursePart?.name;
            if name != (){
                theCourse.name = name;
            }

            int? credit = coursePart?.credit;
            if credit != () {
                theCourse.credit = credit;
            }

            string[]? topics = coursePart?.topics;
            if topics != (){
                theCourse.topics = topics;
            }
            return theCourse;
        }
    }

    resource function delete courses/[string code] () returns CourseNotFound|http:Ok {
        Course? theCourse = self.courses[code];

        if theCourse == () {
            return <CourseNotFound>{
                body: {errorMessage: string `Error no course with code ${code}`}
            };
        }else {
            http:Ok ok = {body: "Deleted Course"};
            return ok;
        }
    }

}
